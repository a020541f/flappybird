
function love.load()
  
  background= love.graphics.newImage("sprites/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
  flappy = love.graphics.newImage("sprites/flappy.png")
  flappyPosY = 200
  
  pipe = love.graphics.newImage("sprites/pipe.png")
  pipePosX = 500
  pipeposY= 0
  
  pipe1 = love.graphics.newImage("sprites/pipe.png")
  pipe1PosX = 750
  pipe1posY = 0
  
  pipe2 = love.graphics.newImage("sprites/piper.png")
  pipe2PosX = 500
  pipe2posY = 0
  
  pipe3 = love.graphics.newImage("sprites/piper.png")
  pipe3PosX = 750
  pipe3posY = 0
  score = 0
  hiscore = 0
  
width1 = math.random(50)
width2 = math.random(50)
  
end

function love.update(dt)

end


function love.draw()

  love.graphics.draw(background, backgroundQuad, 0, 0)
  game_screen()
end

function game_screen()
 
  love.graphics.draw(pipe, pipePosX, 500 + width1)
  love.graphics.draw(pipe1, pipe1PosX, 500 + width2)  
   
  love.graphics.draw(pipe2, pipe2PosX, pipe2posY + width1)


  love.graphics.draw(pipe3, pipe3PosX, pipe3posY + width2)
  love.graphics.draw(flappy, 125, flappyPosY)
  
 velocityX = 2.5;


  pipePosX = pipePosX - 1.5
  pipe1PosX = pipe1PosX - 1.5
  pipe2PosX = pipe2PosX - 1.5
  pipe3PosX = pipe3PosX - 1.5
  
  if (hiscore <= score)
  
  then
    hiscore = score
  end
  
  
  if love.keyboard.isDown("r")
  
  then
    
     flappyPosY = 20
     score = 0
    
    end
  
  if love.keyboard.isDown("up") 
  then
     velocityX = velocityX - 20
    
  if (velocityX < -20)
  
  then
    
    velocityY = -20
    
    end
  
end

if flappyPosY <= 0

then 
  
  flappyPosY = flappyPosY + 100000
  
  end

if flappyPosY >= 750

then 
  
  flappyPosY = flappyPosY + 100000
  
  end
  
  if(pipePosX <= -80)
  
  then
    width1 = math.random(50)

  pipePosX = pipePosX + 500
  
end

if(pipe2PosX <= -80)
  
  then
    
  pipe2PosX = pipe2PosX + 500
  
end

if(pipe1PosX <= -80)
  
  then
    width2 = math.random(50)
  pipe1PosX = pipe1PosX + 500
  
end

if(pipe3PosX <= -80)
  
  then
    
  pipe3PosX = pipe3PosX + 500
  
end

if (flappyPosY <= 10000)

then

if (flappyPosY <= 0)

then
  
  
end

  
if(pipe1PosX <= 130 )
  
  then
    
    if (pipe1PosX >= 129 )
    then
    
  score = score + 1
end

if(pipe2PosX <= 180 )
  
  then
    
    if (pipe2PosX >= 179 )
    then
    
  score = score + 1
  end
end
end

  
end

  
  
  flappyPosY = flappyPosY + velocityX 
  hitTest = CheckCollision(pipePosX, 500 , 60, 150, 125, flappyPosY, 45, 45)
  hitTest1 = CheckCollision(pipe1PosX, 500, 60, 150, 125, flappyPosY, 45, 45)
  hitTest2 = CheckCollision(pipe2PosX, 0 , 60, 150, 125, flappyPosY, 45, 45)
  hitTest3 = CheckCollision(pipe3PosX, 0 , 60, 150, 125, flappyPosY, 45, 45)
  if(hitTest) then
      
    flappyPosY = flappyPosY + 100000
    love.graphics.print("game over", 200, 200)
    
  end
   if(hitTest1) then
      
    flappyPosY = flappyPosY + 100000
    love.graphics.print("game over", 200, 200)
  end
  
  if(hitTest2) then
      
    flappyPosY = flappyPosY + 100000
    love.graphics.print("game over", 200, 200)
  end
  
  if(hitTest3) then
      
    flappyPosY = flappyPosY + 100000
    love.graphics.print("game over", 200, 200)
  end

 love.graphics.print("score:", 20, 40)
 love.graphics.print(score, 60, 40)
 love.graphics.print("highscore:", 20, 80)
 love.graphics.print(hiscore, 88, 80)
  
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
